#include "scanner.h"
#include "regex.h"
#include <iostream>

using namespace std;

/*!This is the main body of the scanner function. */
Token* Scanner::scan(const char *text){
    int lexemeLength = 0;
    int space = 0;
    int line_num = 1;
    int column_num = 1;

    Token* cur = NULL;
    Token* head = cur;
    
    space = consumeWhiteSpaceAndComments(text, line_num, column_num);
    text += space;

    /*!Loops through 'text' and links tokens on the fly */
    while(text[0] != '\0') {
        lexemeLength = 0;
        space = 0;
        Token* temp = getNextToken(text, lexemeLength, space, line_num, column_num);
        //cout<<"After getNextToken Method: line : "<<line_num<<" column :"<<column_num<<endl;
        text += lexemeLength + space;

        if(!cur){
    		cur = temp;
    		head = cur;
    	}else{
    		cur->next = temp;
    		cur = cur->next;
    	}

        column_num += lexemeLength;
    	space = consumeWhiteSpaceAndComments(text, line_num, column_num);
        text += space;
    }
    
    if(!cur) {
		cur = new Token("", endOfFile, NULL, 1, 1);
		head = cur;
	}else {
		cur->next = new Token("", endOfFile, NULL, line_num, column_num);
	} 

    return head;    
}

Token* Scanner::getNextToken(const char *text, int &lexemeLength, int &space, int &Line, int &Column){
    space = consumeWhiteSpaceAndComments(text, Line, Column);
    text += space;

    Token* nextToken = new Token();
    nextToken->terminal = getNextTokenTerminal(text, lexemeLength);
    nextToken->lexeme   = getNextTokenLex(text, lexemeLength);
    nextToken->line     = Line;
    nextToken->column   = Column;
    
    return nextToken;
}

string Scanner::getNextTokenLex(const char *text, int lexemeLength){
    string lex;
    lex.append(text, 0, lexemeLength);
    
    return lex;
}


tokenType Scanner::getNextTokenTerminal (const char *text, int &lexemeLength){
	tokenType terminal = intKwd;
	int numMatchedChars = 0;
	int maxNumMatchedChars = 0;

	for(int i = intKwd; i < endOfFile; i++){
		regex_t* curRegex = regexList.at(i);
		numMatchedChars = matchRegex(curRegex, text);
		if(numMatchedChars > maxNumMatchedChars){
			maxNumMatchedChars = numMatchedChars;
			terminal = (tokenType)i;
            lexemeLength = maxNumMatchedChars;
		}
	}

	if(maxNumMatchedChars==0){
		lexemeLength = 1;
		return lexicalError;
	}

	return terminal;
}


int Scanner::consumeWhiteSpaceAndComments (const char *text, int &line_num, int &column_num) {
    int numMatchedChars = 0;
    int totalNumMatchedChars = 0;
    int stillConsumingWhiteSpace;

    regex_t* whiteSpace = makeRegex("^[\n\t\r ]+");
    regex_t* blockComment = makeRegex("^/\\*([^\\*]|\\*+[^\\*/])*\\*+/");
    regex_t* lineComment = makeRegex("^//[^\n]*\n");
    regex_t* newline = makeRegex("^[\n]+");
    regex_t* tab = makeRegex("^[\t]+");

    do {
        stillConsumingWhiteSpace = 0;  // exit loop if not reset by a match
       
        // Try to match white space
        numMatchedChars = matchRegex (whiteSpace, text);
        totalNumMatchedChars += numMatchedChars;
        if (numMatchedChars > 0) {
            int newLine, Tab; 
            for(int i = 0; i < numMatchedChars; i++){
                newLine = matchRegex (newline, text);
                Tab = matchRegex (tab, text);
                if (newLine > 0){
                    line_num++;
                    column_num = 1;
                }else if(Tab > 0){
                    column_num += 4;
                }else{
                    column_num++;
                }
                text = text + 1;
            }
            stillConsumingWhiteSpace = 1;
        }

        
        // Try to match block comments
        numMatchedChars = matchRegex (blockComment, text);
        totalNumMatchedChars += numMatchedChars;
        if (numMatchedChars > 0) {
            int newLine; 
            for(int i = 0; i < numMatchedChars; i++){
                newLine = matchRegex (newline, text);
                if (newLine > 0){
                    line_num++;
                }
                text = text + 1;
            }
            column_num = 1;
            stillConsumingWhiteSpace = 1;
        } 
        

        // Try to match single-line comments
        numMatchedChars = matchRegex (lineComment, text);
        totalNumMatchedChars += numMatchedChars;
        if (numMatchedChars > 0) {
            text = text + numMatchedChars;
            line_num++;
            column_num = 1;
            stillConsumingWhiteSpace = 1;
        }
    }
    while (stillConsumingWhiteSpace);

    return totalNumMatchedChars;
}

int Scanner::consumeWhiteSpaceAndComments (const char *text) {
    int numMatchedChars = 0;
    int totalNumMatchedChars = 0;
    int stillConsumingWhiteSpace;

    regex_t* whiteSpace = makeRegex("^[\n\t\r ]+");
    regex_t* blockComment = makeRegex("^/\\*([^\\*]|\\*+[^\\*/])*\\*+/");
    regex_t* lineComment = makeRegex("^//[^\n]*\n");

    do {
        stillConsumingWhiteSpace = 0;  // exit loop if not reset by a match

        // Try to match white space
        numMatchedChars = matchRegex (whiteSpace, text);
        totalNumMatchedChars += numMatchedChars;
        if (numMatchedChars > 0) {
            text = text + numMatchedChars;
            stillConsumingWhiteSpace = 1;
        }

        // Try to match block comments
        numMatchedChars = matchRegex (blockComment, text);
        totalNumMatchedChars += numMatchedChars;
        if (numMatchedChars > 0) {
            text = text + numMatchedChars;
            stillConsumingWhiteSpace = 1;
        }

        // Try to match single-line comments
        numMatchedChars = matchRegex (lineComment, text);
        totalNumMatchedChars += numMatchedChars;
        if (numMatchedChars > 0) {
            text = text + numMatchedChars;
            stillConsumingWhiteSpace = 1;
        }
    }
    while (stillConsumingWhiteSpace);

    return totalNumMatchedChars;
}

Token::Token(string lex, tokenType type, Token *nextToken, int line_num, int column_num){
//Token::Token(string lex, tokenType type, Token *nextToken){
    lexeme = lex;
    terminal = type;
    next = nextToken;
    line = line_num;
    column = column_num;
}

Scanner::Scanner() {
    head = NULL;

	/*!When the scanner gets initialized, below regex types are created and inserted into a vector.*/
	
    for(int i=intKwd ; i < endOfFile ; i++){
        switch(i) {
            case intKwd:            regexList.push_back(makeRegex("^Integer")); break;
            case floatKwd:          regexList.push_back(makeRegex("^Float")); break;
            case stringKwd:         regexList.push_back(makeRegex("^String")); break;
            case matrixKwd:         regexList.push_back(makeRegex("^Matrix")); break;
            case letKwd:            regexList.push_back(makeRegex("^let")); break;
            case inKwd:             regexList.push_back(makeRegex("^in")); break;
            case endKwd:            regexList.push_back(makeRegex("^end")); break;
            case ifKwd:             regexList.push_back(makeRegex("^if")); break;
            case thenKwd:           regexList.push_back(makeRegex("^then")); break;
            case elseKwd:           regexList.push_back(makeRegex("^else")); break;
            case forKwd:            regexList.push_back(makeRegex("^for")); break;
            case toKwd:             regexList.push_back(makeRegex("^to")); break;
            case printKwd:          regexList.push_back(makeRegex("^print")); break;

            case intConst:          regexList.push_back(makeRegex("^[0-9]+")); break;
            case floatConst:        regexList.push_back(makeRegex("^[0-9]+\\.[0-9]+")); break;
            case stringConst:       regexList.push_back(makeRegex("^(\"[^\"\r\n]*\")")); break;

            case variableName:      regexList.push_back(makeRegex("^([a-zA-Z]+[_a-zA-Z0-9]*)")); break;

            case leftParen:         regexList.push_back(makeRegex("^\\(")); break;
            case rightParen:        regexList.push_back(makeRegex("^\\)")); break;
            case leftCurly:         regexList.push_back(makeRegex("^\\{")); break;
            case rightCurly:        regexList.push_back(makeRegex("^\\}")); break;
            case leftSquare:        regexList.push_back(makeRegex("^\\[")); break;
            case rightSquare:       regexList.push_back(makeRegex("^\\]")); break;

            case comma:             regexList.push_back(makeRegex("^,")); break;
            case semiColon:         regexList.push_back(makeRegex("^;")); break;

            case assign:            regexList.push_back(makeRegex("^=")); break;
            case plusSign:          regexList.push_back(makeRegex("^\\+")); break;
            case star:              regexList.push_back(makeRegex("^\\*")); break;
            case dash:              regexList.push_back(makeRegex("^-")); break;
            case forwardSlash:      regexList.push_back(makeRegex("^/")); break;
            case lessThan:          regexList.push_back(makeRegex("^<")); break;
            case lessThanEqual:     regexList.push_back(makeRegex("^<=")); break;
            case greaterThan:       regexList.push_back(makeRegex("^>")); break;
            case greaterThanEqual:  regexList.push_back(makeRegex("^>=")); break;
            case equalsEquals:      regexList.push_back(makeRegex("^==")); break;
            case notEquals:         regexList.push_back(makeRegex("^!=")); break;
            case andOp:             regexList.push_back(makeRegex("^&&")); break;
            case orOp:              regexList.push_back(makeRegex("^\\|\\|")); break;
            case notOp:             regexList.push_back(makeRegex("^!")); break;
            case endOfFile:; break;
            case lexicalError:; break;
        }
    }    
}


Scanner::~Scanner(){
    Token* temp = head;
    while(head != NULL){
        while(temp->next != NULL){
            temp = temp->next;
            if(temp->next == NULL){
                delete[] temp;
            }
        }
        if(head->next == NULL){
            delete[] head;
        }
    }

}
