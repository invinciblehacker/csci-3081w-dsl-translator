/*!
 * @file AST.h
 * Language:    C++
 * 
 *              @author Jangyoon Kim (kimx2873@umn.edu)
 *              @author Torry Tufteland (tufte017@umn.edu)
 *              @date 11/20/2013
 * 
 * Description: @brief DSL Translator -- Abstract Syntax Tree
 *              @details This file is class declaration for AST.
 *                     
 * 
*/
#ifndef Node_H
#define Node_H

#include <string>
#include <iostream> 
#include <sstream>
#include "scanner.h"

using namespace std;

class Node;
class Stmts;
class Stmt;
class Expr;
class VarName;

/*! 
 * @class 		Node
 * @brief		The root of the AST-tree that all the classes inherits from.
 */
class Node {
	public:
		virtual std::string unparse ( ) { return " This should be pure virtual" ;};
		virtual std::string cppCode ( ) { return " This should be pure virtual" ;};
		virtual ~Node(){}
};


/*! 
 * @class 		Program
 * @brief 		Abstract class for program.
 * 				Program::= VarName() { Stmts }
 */
class Program : public Node {
	private:
		VarName *var;
		Stmts *sts;
	public:
		Program(VarName*,Stmts*);
		~Program();
		string unparse ();
		string cppCode ();
};

/*! 
 * @class 		Stmts
 * @brief 		Abstract class for all 'Stmts' productions.
 */
class Stmts : public Node {
	public:
		Stmts(){}
		~Stmts(){}
};

/*! 
 * @class 		nilStmts
 * @brief 		Class for empty 'Stmts'.
 */
class nilStmts : public Stmts {
	public:
		nilStmts(){}
		virtual ~nilStmts(){};
		string unparse(){return "";}
		string cppCode(){return "";}
};

/*! 
 * @class 		consStmts
 * @brief 		Class for non-empty Stmts productions.
 *				Stmts::= Stmt Stmts
 */
class consStmts : public Stmts {
	public:
		consStmts(Stmt*, Stmts*);
		~consStmts();
		string unparse();
		string cppCode();
	private:
		Stmt * s;
		Stmts * ss;
};

/*! 
 * @class 		Stmt
 * @brief 		Abstract class for 'Stmt' productions.
 */
class Stmt : public Node {
	public:
		Stmt(){};
		virtual ~Stmt(){};
};

/*! 
 * @class 		BlockStmts
 * @brief 		Class for blockStmts productions.
 *				Stmt::= '{' Stmts '}'
 */
class BlockStmts : public Stmt {
	public:
		BlockStmts(Stmts*);
		~BlockStmts();
		string unparse();
		string cppCode();
	private:
		Stmts* sts;
};

/*! 
 * @class 		IfStmt
 * @brief 		Class for ifStmt productions.
 *				Stmt::= 'if''(' Expr ')' Stmt
 */
class IfStmt : public Stmt {
	public:
		IfStmt(Expr*, Stmt*);
		~IfStmt();
		string unparse();
		string cppCode();
	private:
		Expr* e;
		Stmt* st;
};

/*! 
 * @class 		IfElseStmt
 * @brief 		Class for ifelseStmt productions.
 *				Stmt::= 'if''(' Expr ')' Stmt 'else' Stmt
 */
class IfElseStmt : public Stmt {
	public:
		IfElseStmt(Expr*, Stmt*, Stmt*);
		~IfElseStmt();
		string unparse();
		string cppCode();
	private:
		Expr* e;
		Stmt* st1;
		Stmt* st2;
};

/*! 
 * @class 		ForStmt
 * @brief 		Class for forStmt productions.
 *				Stmt::= 'for''(' varName '=' Expr 'to' Expr ')' Stmt
 */
class ForStmt : public Stmt {
	public:
		ForStmt(string, Expr*, Expr*, Stmt*);
		~ForStmt();
		string unparse();
		string cppCode();
	private:
		string vr;
		Expr* e1;
		Expr* e2;
		Stmt* s;
};

/*! 
 * @class 		VarNameMatrixStmt
 * @brief 		Class for VarNameMatrixStmt productions.
 *				Stmt::= varName '[' Expr ',' Expr ']' '=' Expr ';'
 */
class VarNameMatrixStmt : public Stmt {
	public:
		VarNameMatrixStmt(string, Expr*, Expr*, Expr*);
		~VarNameMatrixStmt();
		string unparse();
		string cppCode();
	private:
		string vr;
		Expr* e1;
		Expr* e2;
		Expr* e3;
};

/*! 
 * @class 		VarNameStmt
 * @brief 		Class for VarNameStmt productions.
 *				Stmt::= varName '=' Expr ';'
 */
class VarNameStmt : public Stmt {
	public:
		VarNameStmt(string, Expr*);
		~VarNameStmt();
		string unparse();
		string cppCode();
	private:
		string vr;
		Expr* e;
};

/*! 
 * @class 		PrintStmt
 * @brief 		Class for PrintStmt productions.
 *				Stmt::= 'print''(' Expr ')' ';'
 */
class PrintStmt : public Stmt {
	public:
		PrintStmt(Expr*);
		~PrintStmt();
		string unparse();
		string cppCode();
	private:
		Expr* e;
};

/*! 
 * @class 		SemiStmt
 * @brief 		Class for SemiStmt productions.
 *				Stmt::= ';'
 */
class SemiStmt : public Stmt {
	public:
		SemiStmt(){};
		~SemiStmt(){};
		string unparse();
		string cppCode();
};

/*! 
 * @class 		StdDecl
 * @brief 		Concrete class for standard declarations.
 *				Decl ::= integerKwd varName';'
 *				Decl ::= floatKwd varName';'
 *				Decl ::= stringKwd varName';'
 */
class StdDecl : public Node {
	public:
		StdDecl(string, string);
		~StdDecl();
		string unparse();
		string cppCode();
	private:
		string var;
		string type;
};

/*! 
 * @class 		MatrixInit
 * @brief 		Class for Matrix initialization.
 *				Decl::= Matrix 'VarName' = 'Expr';
 */
class MatrixInit : public Node {
	public:
		MatrixInit(VarName*, Expr*);
		~MatrixInit();
		string unparse();
		string cppCode();
	private:
		VarName* var;
		Expr* opr;
};

/*! 
 * @class 		MatrixSetter
 * @brief 		Class for Matrix initialization with some operations.
 *				Decl::= Matrix 'VarName''[' Expr ',' Expr ']' varName ',' varName = 'Expr';
 */
class MatrixSetter : public Node {
	public:
		MatrixSetter(VarName*, Expr*, Expr*, VarName*, VarName*, Expr*);
		~MatrixSetter();
		string unparse();
		string cppCode();
	private:
		VarName* var1;
		VarName* var2;
		VarName* var3;
		Expr* opr1;
		Expr* opr2;
		Expr* opr3;
};

/*! 
 * @class 		Expr
 * @brief 		Abstract class for all 'Expr' productions.
 */
class Expr : public Node {
	public:
		Expr(){};
		virtual ~Expr(){};
};

/*! 
 * @class 		Constant
 * @brief 		Class for Constant productions.
 *				Expr ::= integerConst | floatConst | stringConst
 */
template<class C> 
class Constant : public Expr {
	public:
		Constant(string c){
			constant = c;
		}
		string cppCode(){
			return constant;
		}
		string unparse(){
			return constant;
		}
	private:
		string constant;
};

/*! 
 * @class 		Addition
 * @brief 		Class for Addition productions.
 *				Expr ::= Expr '+' Expr
 */
class Addition : public Expr {
	public:
		Addition(Expr*, Expr*);
		~Addition();
		string unparse();
		string cppCode();
	private:
		Expr* opr1;
		Expr* opr2;
};

/*! 
 * @class 		Subtraction
 * @brief 		Class for Subtraction productions.
 *				Expr ::= Expr '-' Expr
 */
class Subtraction : public Expr {
	public:
		Subtraction(Expr*, Expr*);
		~Subtraction();
		string unparse();
		string cppCode();
	private:
		Expr* opr1;
		Expr* opr2;
};

/*! 
 * @class 		Multiplication
 * @brief 		Class for Multiplication productions.
 *				Expr ::= Expr '*' Expr
 */
class Multiplication : public Expr {
	public:
		Multiplication(Expr*, Expr*);
		~Multiplication();
		string unparse();
		string cppCode();
	private:
		Expr* opr1;
		Expr* opr2;
};

/*! 
 * @class 		Division
 * @brief 		Class for Division productions.
 *				Expr ::= Expr '/' Expr
 */
class Division : public Expr {
	public:
		Division(Expr*, Expr*);
		~Division();
		string unparse();
		string cppCode();
	private:
		Expr* opr1;
		Expr* opr2;
};

/*! 
 * @class 		VarName
 * @brief 		Class for VarName productions.
 *				Expr ::= varName
 */
class VarName : public Expr {
	public:
		VarName(string);
		~VarName();
		string unparse();
		string cppCode();
	private:
		string var;
};

/*! 
 * @class 		MatrixREF
 * @brief 		Class for MatrixREF productions.
 *				Expr ::= varName '[' Expr ',' Expr ']'
 */
class MatrixREF : public Expr {
	public:
		MatrixREF(string, Expr*, Expr*);
		~MatrixREF();
		string unparse();
		string cppCode();
	private:
		Expr* opr1;
		Expr* opr2;
		string s;
};

/*! 
 * @class 		VarNameExpr
 * @brief 		Class for VarNameExpr productions.
 *				Expr ::= varName '(' Expr ')'
 */
class VarNameExpr : public Expr {
	public:
		VarNameExpr(string, Expr*);
		~VarNameExpr();
		string unparse();
		string cppCode();
	private:
		Expr* opr;
		string s;
};

/*! 
 * @class 		lrParen
 * @brief 		Class for left&right Paren productions.
 *				Expr ::= '(' Expr ')'
 */
class lrParen : public Expr {
	public:
		lrParen(Expr*);
		~lrParen();
		string unparse();
		string cppCode();
	private:
		Expr* opr;
};

/*! 
 * @class 		Assign
 * @brief 		Class for Assign productions.
 *				Expr ::= varName '=' Expr ';'
 */
class Assign : public Stmt {
	public:
		Assign(string, Expr*);
		~Assign();
		string unparse();
		string cppCode();
	private:
		string v;
		Expr* e;
};

/*! 
 * @class 		RelationalExpr
 * @brief 		Class for RelationalExpr productions.
 *				Expr ::= Expr '>' Expr
 *				Expr ::= Expr '>=' Expr
 *				Expr ::= Expr '<' Expr
 *				Expr ::= Expr '<=' Expr
 *				Expr ::= Expr '==' Expr
 *				Expr ::= Expr '!=' Expr
 *				Expr ::= Expr '&&' Expr
 *				Expr ::= Expr '||' Expr
 */
class RelationalExpr : public Expr {
	public:
		RelationalExpr(Expr*, Expr*, string);
		~RelationalExpr();
		string unparse();
		string cppCode();
	private:
		Expr* opr1;
		Expr* opr2;
		string relOp;
};

/*! 
 * @class 		NotOp
 * @brief 		Class for Not operator productions.
 *				Expr ::= '!' Expr
 */
class NotOp : public Expr {
	public:
		NotOp(Expr*);
		~NotOp();
		string unparse();
		string cppCode();
	private:
		Expr* opr;
};

/*! 
 * @class 		IfThenElse
 * @brief 		Class for IfThenElse productions.
 *				Expr::= 'if' Expr 'then' Expr 'else' Expr
 */
class IfThenElse : public Stmt {
	public:
		IfThenElse(Expr*, Expr*, Expr*);
		~IfThenElse();
		string unparse();
		string cppCode();
	private:
		Expr* opr1;
		Expr* opr2;
		Expr* opr3;
};

/*! 
 * @class 		LetExpr
 * @brief 		Class for LetExpr productions.
 *				Expr::= 'let' Stmts 'in' Expr 'end'
 */
class LetExpr : public Expr {
	public:
		LetExpr(Stmts*, Expr*);
		~LetExpr();
		string unparse();
		string cppCode();
	private:
		Stmts* ss;
		Expr * e;
};

#endif
