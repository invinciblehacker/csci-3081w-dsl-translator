#include <string>
#include <iostream>
#include <sstream>
#include "scanner.h"
#include "AST.h"

using namespace std;

/*!
 * Below are the class definitions for both abstract classes and concrete classes
 * */
string Program::unparse(){
	stringstream ss;
	ss << var->unparse() << "()" << "{" << sts->unparse() << "}";
	return ss.str();
}

string Program::cppCode(){
	stringstream ss;
	ss << "#include <iostream>\n#include \"Matrix.h\"\n#include <math.h>\nusing namespace std; \n\n" \
		<< "int "<<var->cppCode() << "()" << "{\n" << sts->cppCode() << "}";
	return ss.str();
}

Program::Program(VarName* varx, Stmts* ssx) {
	var = varx;
	sts = ssx;
}

Program::~Program() {
	delete var;
	delete sts;
}

/*
 * STATEMENTS
 * */

consStmts::consStmts(Stmt* s, Stmts* ss) {
	this->s = s;
	this->ss = ss;
}

consStmts::~consStmts(){
	delete s;
	delete ss;
}

string consStmts::unparse(){
    stringstream str;
    str << s->unparse() << ss->unparse();
    return str.str();
}

string consStmts::cppCode(){
	stringstream str;
	str << "\t" << s->cppCode() << ss->cppCode();
	return str.str();
}

BlockStmts::BlockStmts(Stmts* st){
	sts = st;
}

BlockStmts::~BlockStmts(){
	delete sts;
}

string BlockStmts::unparse(){
	stringstream ss;
	ss << "{" << sts->unparse() << "}";
	return ss.str();
}

string BlockStmts::cppCode(){
	stringstream ss;
	ss << "{\n\t" << sts->cppCode() << "\t}\n";
	return ss.str();
}

IfStmt::IfStmt(Expr* ex, Stmt* St){
	e = ex;
	st = St;
}

IfStmt::~IfStmt(){
	delete e;
	delete st;
}

string IfStmt::unparse(){
	stringstream ss;
	ss << "if (" << e->unparse() << ")" << st->unparse();
	return ss.str();
}

string IfStmt::cppCode(){
	stringstream ss;
	ss << "if (" << e->cppCode() << ")" << st->cppCode();
	return ss.str();
}

IfElseStmt::IfElseStmt(Expr* ex, Stmt* St1, Stmt* St2){
	e = ex;
	st1 = St1;
	st2 = St2;
}

IfElseStmt::~IfElseStmt(){
	delete e;
	delete st1;
	delete st2;
}

string IfElseStmt::unparse(){
	stringstream ss;
	ss << "if (" << e->unparse() << ")" << st1->unparse() << "else" << st2->unparse();
	return ss.str(); 	
}

string IfElseStmt::cppCode(){
	stringstream ss;
	ss << "if (" << e->cppCode() << ") \n\t" << st1->cppCode() << "\t else \n\t" << st2->cppCode() << "\n";
	return ss.str(); 	
}

ForStmt::ForStmt(string vv, Expr* ex1, Expr* ex2, Stmt* st){
	vr = vv;
	e1 = ex1;
	e2 = ex2;
	s = st;
}

ForStmt::~ForStmt(){
	vr = "";	
	delete e1;
	delete e2;
	delete s;	
}

string ForStmt::unparse(){
	stringstream ss;
	ss << "for (" << vr << " = " << e1->unparse() << " to " << e2->unparse() << ")" << s->unparse();
	return ss.str();
}

string ForStmt::cppCode(){
	stringstream ss;
	ss << "for (" << vr << " = " << e1->cppCode() << "; " << vr << " <= "  << e2->cppCode() << "; "<< vr << "++" << ")" << s->cppCode();
	return ss.str();
}

VarNameMatrixStmt::VarNameMatrixStmt(string var, Expr* s1, Expr* s2, Expr* s3){
	vr = var;
	e1 = s1;
	e2 = s2;
	e3 = s3;
}

VarNameMatrixStmt::~VarNameMatrixStmt(){
	vr = "";
	delete e1;
	delete e2;
	delete e3;	
}

string VarNameMatrixStmt::unparse(){
	stringstream ss;
	ss << vr << "[" << e1->unparse() << ", " << e2->unparse() << "] = " << e3->unparse() << ";";
	return ss.str(); 
}

string VarNameMatrixStmt::cppCode(){
	stringstream ss;
	ss << "*(" << vr << ".access(" << e1->cppCode() << ", " << e2->cppCode() << ")) =" << e3->cppCode()<< ";\n";
	return ss.str(); 
}

VarNameStmt::VarNameStmt(string var, Expr* s){
	vr = var;
	e = s;
}

VarNameStmt::~VarNameStmt(){
	vr = "";
	delete e;	
}

string VarNameStmt::unparse(){
	stringstream ss;
	ss << vr << " = " << e->unparse() << ";";
	return ss.str(); 
}

string VarNameStmt::cppCode(){
	stringstream ss;
	ss << vr << " = " << e->cppCode() << ";\n";
	return ss.str(); 
}

PrintStmt::PrintStmt(Expr* d){
	e = d;
}

PrintStmt::~PrintStmt(){
	delete e;
}

string PrintStmt::unparse(){
	stringstream ss;
	ss << "print (" << e->unparse() << ");";
	return ss.str();
}

string PrintStmt::cppCode(){
	stringstream ss;
	ss << "cout << " << e->cppCode() << ";\n";
	return ss.str();
}

string SemiStmt::unparse(){
	stringstream ss;
	ss << ";\n";
	return ss.str();
}

string SemiStmt::cppCode(){
	stringstream ss;
	ss << ";\n";
	return ss.str();
}

/*
 * DECLARATIONS
 * */
StdDecl::StdDecl(string typex, string varx){
	type = typex;
	var = varx; 
}

StdDecl::~StdDecl(){
	type = "";
	var = "";
}

string StdDecl::unparse(){
	stringstream ss;
	ss << type << " " << var << ";";
	return ss.str();
}

string StdDecl::cppCode(){
	stringstream ss;
	
	if(type == "Integer") {
		type = "int"; 
	}else if(type == "Float") {
		type = "float";
	}else{
		type = "string";
	}
		
	ss << type << " " << var << ";\n";
	return ss.str();
}
 
MatrixInit::MatrixInit(VarName* varx, Expr* oprx) {
	var = varx;
	opr = oprx;
}

MatrixInit::~MatrixInit() {
	delete var;
	delete opr;
}

string MatrixInit::unparse() {
	stringstream ss;
	ss << "Matrix " << var->unparse() << " = " << opr->unparse() << ";";
	return ss.str();
}	

string MatrixInit::cppCode() {
	stringstream ss;
	ss << "Matrix " << var->cppCode() << " = " << var->cppCode() << "." << opr->cppCode() << ";\n";
	return ss.str();
}
	
MatrixSetter::MatrixSetter(VarName* var1x, Expr* opr1x, Expr* opr2x, VarName* var2x, VarName* var3x, Expr* opr3x) {
	var1 = var1x;
	var2 = var2x;
	var3 = var3x;
	opr1 = opr1x;
	opr2 = opr2x;
	opr3 = opr3x;
}

MatrixSetter::~MatrixSetter() {
	delete var1;
	delete var2;
	delete var3;
	delete opr1;
	delete opr2;
	delete opr3;
}

string MatrixSetter::unparse() {
	stringstream ss;
	ss << "Matrix " << var1->unparse() << "[" << opr1->unparse() << ", " << opr2->unparse() \
				<< "]" << var2->unparse() << ", " << var3->unparse() << " = " << opr3->unparse() << ";";
	return ss.str();
}

string MatrixSetter::cppCode() {
	stringstream ss;
	ss << "Matrix " << var1->cppCode() << "(" << opr1->cppCode() << ", " << opr2->cppCode() << ");\n\t" \
	<< "for(int " << var2->cppCode() << " = 0; " << var2->cppCode() << " < " << opr1->cppCode() << "; " << var2->cppCode() \
	<< "++){\n\t\t" << "for(int " << var3->cppCode() << " = 0; " << var3->cppCode() << " < " << opr2->cppCode() << "; " \
	<< var3->cppCode() << "++){\n\t\t\t" << "\t" << "*(" << var1->cppCode() \
	<< ".access(" << var2->cppCode() <<", " << var3->cppCode() << ")) = "<< opr3->cppCode() <<";" << "\n\t\t}" << "\n\t}\n";
	return ss.str();
}


/*
 * EXPRESSIONS
 * */
Addition::Addition(Expr* e1, Expr* e2){
	opr1 = e1;
	opr2 = e2;
}

Addition::~Addition(){
	delete opr1;
	delete opr2;
}

string Addition::unparse() {
	stringstream ss;
	ss << opr1->unparse() << " + " << opr2->unparse() ;
	return ss.str();
}

string Addition::cppCode() {
	stringstream ss;
	ss << "(" << opr1->cppCode() << " + " << opr2->cppCode() << ")" ;
	return ss.str();
}

Subtraction::Subtraction(Expr* e1, Expr* e2){
	opr1 = e1;
	opr2 = e2;
}

Subtraction::~Subtraction(){
	delete opr1;
	delete opr2;
}

string Subtraction::unparse() {
	stringstream ss;
	ss << opr1->unparse() << " - " << opr2->unparse() ;
	return ss.str();
}

string Subtraction::cppCode() {
	stringstream ss;
	ss << "(" << opr1->cppCode() << " - " << opr2->cppCode() << ")" ;
	return 
	ss.str();
}

Multiplication::Multiplication(Expr* e1, Expr* e2){
	opr1 = e1;
	opr2 = e2;
}

Multiplication::~Multiplication(){
	delete opr1;
	delete opr2;
}

string Multiplication::unparse() {
	stringstream ss;
	ss << opr1->unparse() << " * " << opr2->unparse() ;
	return ss.str();
}

string Multiplication::cppCode() {
	stringstream ss;
	ss << "(" << opr1->cppCode() << " * " << opr2->cppCode() << ")";
	return ss.str();
}

Division::Division(Expr* e1, Expr* e2){
	opr1 = e1;
	opr2 = e2;
}

Division::~Division(){
	delete opr1;
	delete opr2;	
}

string Division::unparse() {
	stringstream ss;
	ss << opr1->unparse() << " / " << opr2->unparse() ;
	return ss.str();
}

string Division::cppCode() {
	stringstream ss;
	ss << "(" << opr1->cppCode() << " / " << opr2->cppCode() << ")";
	return ss.str();
}

VarName::VarName(string s){
	var = s;
}

VarName::~VarName(){
	var = "";
}

string VarName::unparse(){
	stringstream ss;
	ss << var;
	return ss.str();
}

string VarName::cppCode(){
	stringstream ss;
	ss << var;
	return ss.str();
}

MatrixREF::MatrixREF(string var, Expr* e1, Expr* e2){
	opr1 = e1;
	opr2 = e2;
	s = var;
}

MatrixREF::~MatrixREF(){
	delete opr1;
	delete opr2;
	s = "";
}

string MatrixREF::unparse(){
	stringstream ss;
	ss << s << "[" << opr1->unparse() << ", " << opr2->unparse() << "]";
	return ss.str();
}

string MatrixREF::cppCode(){
	stringstream ss;
	ss << "*(" << s << ".access(" << opr1->cppCode() << ", " << opr2->cppCode() << "))";
	return ss.str();
}

VarNameExpr::VarNameExpr(string sa, Expr* e){
	opr = e;
	s = sa;
}

VarNameExpr::~VarNameExpr(){
	delete opr;
	s = "";
}

string VarNameExpr::unparse(){
	stringstream ss;
	ss << s << "(" << opr->unparse() << ")";
	return ss.str();
}

string VarNameExpr::cppCode(){
	stringstream ss;
	ss << s << "(" << opr->cppCode() << ")";
	return ss.str();
}

lrParen::lrParen(Expr* e){
	opr = e;
}

lrParen::~lrParen(){
	delete opr;
}

string lrParen::unparse(){
	stringstream ss;
	ss << "(" << opr->unparse() << ")";
	return ss.str();
}

string lrParen::cppCode(){
	stringstream ss;
	ss << "(" << opr->cppCode() << ")";
	return ss.str();
}

IfThenElse::IfThenElse(Expr* e1, Expr* e2, Expr* e3){
	opr1 = e1;
	opr2 = e2;
	opr3 = e3;
}

IfThenElse::~IfThenElse(){
	delete opr1;
	delete opr2;
	delete opr3;	
}

string IfThenElse::unparse() {
	stringstream ss;
	ss << "if " << opr1->unparse() << " then " << opr2->unparse() << " else " << opr3->unparse();
	return ss.str();
}

string IfThenElse::cppCode() {
	stringstream ss;
	ss << "(" << opr1->cppCode() << " ? " << opr2->cppCode() << " : " << opr3->cppCode() << ")";
	return ss.str();
}

NotOp::NotOp(Expr* e){
	opr = e;
}

NotOp::~NotOp(){
	delete opr;	
}

string NotOp::unparse() {
	stringstream ss;
	ss << "!" << opr->unparse();
	return ss.str();
}

string NotOp::cppCode() {
	stringstream ss;
	ss << "!(" << opr->cppCode() << ")";
	return ss.str();
}

Assign::Assign(string var, Expr* expr){
	v = var;
    e = expr;
}

Assign::~Assign(){
	delete e;
	v = "";
}

string Assign::unparse() {
    stringstream ss;
    ss << v << " = " << e->unparse() ;
    return ss.str();
}

string Assign::cppCode(){
	stringstream ss;
	ss << v << " = (" << e->cppCode() << ")";
    return ss.str();
}

RelationalExpr::RelationalExpr(Expr* e1, Expr* e2, string op) {
	opr1 = e1;
	opr2 = e2;
	relOp = op;
}
RelationalExpr::~RelationalExpr() {
	delete opr1;
	delete opr2;
	relOp = "";
}

string RelationalExpr::unparse() {
	stringstream ss;
	ss << opr1->unparse() << " " << relOp << " " << opr2->unparse() ;
	return ss.str();
}

string RelationalExpr::cppCode() {
	stringstream ss;
	ss << "(" << opr1->cppCode() << " " << relOp << " " << opr2->cppCode() << ")";
	return ss.str();
}
 
LetExpr::LetExpr(Stmts* ss, Expr* e) {
	this->ss = ss;
	this->e = e;
}

LetExpr::~LetExpr(){
	delete ss;
	delete e;
}

string LetExpr::unparse() {
    stringstream str;
    str << "let " << ss->unparse() << " in " << e->unparse() << " end";
    return str.str();
 }
 
string LetExpr::cppCode() {
    stringstream str;
    str << "({\n\t" << ss->cppCode() << e->cppCode() << ";\n\t})";
    return str.str();
 }
