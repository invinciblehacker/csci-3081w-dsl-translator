/**
 * @file parser.cpp
 * Language:    C++
 *
 *              @author Jangyoon Kim (kimx2873@umn.edu)
 *              @author Torry Tufteland (tufte017@umn.edu)
 *              @date 11/20/2013
 * 
 * Description: @brief DSL Translator -- parser of the translator.
 *              @details This file is the definition of the Parser class (body file).
 *                     
 * 
 */
#include "parser.h"
#include "scanner.h"
#include "extToken.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
using namespace std ;

/**
 * @brief       Destructor for Parser
 * @see         Parser::~Parser()
 * @return      void
 */
Parser::~Parser() {
    if (s) delete s ;

    ExtToken *extTokenToDelete ;
    currToken = tokens ;
    while (currToken) {
        extTokenToDelete = currToken ;
        currToken = currToken->next ;
        delete extTokenToDelete ;
    }

    Token *tokenToDelete ;
    Token *currScannerToken = stokens ;
    while (currScannerToken) {
        tokenToDelete = currScannerToken ;
        currScannerToken = currScannerToken->next ;
        delete tokenToDelete ;
    }

}

/**
 * @brief       Default constructor for Parser
 * @see         Parser::Parser()
 * @return      void
 */
Parser::Parser ( ) { 
    currToken = NULL; prevToken = NULL ; tokens = NULL; 
    s = NULL; stokens = NULL; 
}

/**
 * @see         Parser::parse(const char *text)
 * @brief       This is the main method of parser. This will call other methods to complete the parse tree.
 *              (This is the very top of the parse tree)
 * @param[in]   text -- The input is the actual text the parser is trying to parse.
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parse (const char *text) {
    assert (text != NULL) ;

    ParseResult pr ;
    try {
        s = new Scanner() ;
        stokens = s->scan (text) ; 
        tokens = extendTokenList ( this, stokens ) ;
	
        assert (tokens != NULL) ;
        currToken = tokens ;
        pr = parseProgram( ) ;
    }
    catch (string errMsg) {
        pr.ok = false ;
        pr.errors = errMsg ;
        pr.ast = NULL ;
    }
    return pr ;
}

/*
 * parse methods for non-terminal symbols
 * --------------------------------------
 */

/**
 * @see         Parser::parseProgram()
 * @brief       This method matches the very beginning of the program.
 *              Program ::= nameKwd colon variableName semiColon Platform Decls States
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseProgram () {
    ParseResult pr ;

    // Program ::= nameKwd colon variableName semiColon Platform Decls States
    match(variableName) ;
    string name( prevToken->lexeme ) ;
    VarName* varname = new VarName(name);
    match(leftParen);
    match(rightParen);
    match(leftCurly);
    ParseResult prStmts = parseStmts() ;
    Stmts* s = NULL;
    if(prStmts.ast){
		s = dynamic_cast<Stmts*>(prStmts.ast);
		if(!s)throw((string) "Bad cast of State in parseProgram");
	}
    match(rightCurly);
    match(endOfFile) ;
    pr.ast = new Program(varname,s);
    
    return pr ;
}

/**
 * @see         Parser::parseMatrixDecl()
 * @brief       This method matches Matrix functions.
 *              Decl ::= 'Matrix' varName '[' Expr ',' Expr ']' varName ',' varName  '=' Expr ';'
 *              Decl ::= 'Matrix' varName '=' Expr ';'
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseMatrixDecl () {
    ParseResult pr,pr2,pr3,pr4,pr5;
    match(matrixKwd);
    match(variableName);
	VarName* var = new VarName( prevToken->lexeme.c_str() );
    // Decl ::= 'Matrix' varName '[' Expr ',' Expr ']' varName ',' varName  '=' Expr ';'
    if(attemptMatch(leftSquare)){
        pr = parseExpr(0);
        match(comma);
        pr2 = parseExpr(0);
        match(rightSquare);
        pr3 = parseVariableName();
        match(comma);
        pr4 = parseVariableName();
        match(assign);
        pr5 = parseExpr(0);
        pr.ast = new MatrixSetter(var, (Expr*)pr.ast, (Expr*)pr2.ast, (VarName*)pr3.ast, (VarName*)pr4.ast, (Expr*)pr5.ast);
    }
    // Decl ::= 'Matrix' varName '=' Expr ';'
    else if(attemptMatch(assign)){
        pr = parseExpr(0);
        
        pr.ast = new MatrixInit(var, (Expr*)pr.ast);
    }
    else{
        throw ( (string) "Bad Syntax of Matrix Decl in in parseMatrixDecl" ) ;
    }   

    match(semiColon) ;

    return pr ;
}

/**
 * @see         Parser::parseStandardDecl()
 * @brief       This method matches variable types.
 *              Type ::= intKwd
 *              Type ::= floatKwd
 *              Type ::= stringKwd 
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseStandardDecl(){
    ParseResult pr ;
    string type;
    string var;
    // ParseResult prType = parseType() ;

    if ( attemptMatch(intKwd) ) {
        // Type ::= intKwd
        type = "Integer";
    } 
    else if ( attemptMatch(floatKwd) ) {
        // Type ::= floatKwd
        type = "Float";
    }
    else if ( attemptMatch(stringKwd) ) {
        // Type ::= stringKwd
        type = "String";
    }
    match(variableName) ;
    var = prevToken->lexeme.c_str();
    match(semiColon) ;
    pr.ast = new StdDecl(type, var);
    return pr ;
}

/**
 * @see         Parser::parseDecl()
 * @brief       This method determines whether it should call MatrixDecl or StandardDecl,
 *              depending on the matched word.
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseDecl () {
    ParseResult pr ;
    // Decl :: Matrix variableName ....
    if(nextIs(matrixKwd)){
        pr = parseMatrixDecl();
    } 
    // Decl ::= Type variableName semiColon
    else{
        pr = parseStandardDecl();
    }
    return pr ;
}

/**
 * @see         Parser::parseStmts()
 * @brief       This method determines whether it should call nilStmts (or empty stmt) or consStmts (or stmts),
 *              depending on the matched word.
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseStmts () {
    ParseResult pr;
    if ( ! nextIs(rightCurly) && !nextIs(inKwd)  ) {
        // Stmts ::= Stmt Stmts
        ParseResult prStmt = parseStmt() ;
        ParseResult prStmts = parseStmts() ;
        pr.ast = new consStmts((Stmt*) prStmt.ast, (Stmts*) prStmts.ast);
    }
    else {
        // Stmts ::= NULL
        pr.ast = new nilStmts();
    }
    return pr ;
}


/**
 * @see         Parser::parseStmt()
 * @brief       This method determines whether it should call one of the methods below,
 *              depending on the matched word.
 *              Stmt ::= Decl
 *              Stmt ::= '{' Stmts '}'
 *              Stmt ::= 'if' '(' Expr ')' Stmt
 *              Stmt ::= 'if' '(' Expr ')' Stmt 'else' Stmt
 *              Stmt ::= varName '=' Expr ';'  | varName '[' Expr ',' Expr ']' '=' Expr ';'
 *              Stmt ::= 'print' '(' Expr ')' ';'
 *              Stmt ::= 'for' '(' varName '=' Expr 'to' Expr ')' Stmt
 *              Stmt ::= ';'
 *              Stmt ::= variableName assign Expr semiColon
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseStmt () {
    ParseResult pr, pr1, pr2;
	string var;
	
    // Stmt ::= Decl
    if(nextIs(intKwd)||nextIs(floatKwd)||nextIs(matrixKwd)||nextIs(stringKwd)){
        pr = parseDecl();
    }
    // Stmt ::= '{' Stmts '}'
    else if (attemptMatch(leftCurly)){
        pr = parseStmts();
        match(rightCurly);
        pr.ast = new BlockStmts((Stmts*) pr.ast);
    }   
    // Stmt ::= 'if' '(' Expr ')' Stmt
    // Stmt ::= 'if' '(' Expr ')' Stmt 'else' Stmt
    else if (attemptMatch(ifKwd)){
        match(leftParen);
        pr = parseExpr(0);
        match(rightParen);
        pr1 = parseStmt();
        
        if(attemptMatch(elseKwd)){
            pr2 = parseStmt();
            pr.ast = new IfElseStmt((Expr*) pr.ast, (Stmt*) pr1.ast, (Stmt*) pr2.ast);
            return pr;
        }
        
        pr.ast = new IfStmt((Expr*) pr.ast, (Stmt*) pr1.ast);
    }
    // Stmt ::= varName '=' Expr ';'  | varName '[' Expr ',' Expr ']' '=' Expr ';'
    else if  ( attemptMatch (variableName) ) {
		var = prevToken->lexeme.c_str();
		
        if (attemptMatch ( leftSquare ) ) {
            pr = parseExpr(0);
            match ( comma ) ;
            pr1 = parseExpr(0) ;
            match  ( rightSquare ) ;
            match(assign);
			pr2 = parseExpr(0);
			match(semiColon);

			pr.ast = new VarNameMatrixStmt(var, (Expr*) pr.ast, (Expr*) pr1.ast, (Expr*) pr2.ast);
        }else{
			match(assign);
			pr = parseExpr(0);
			match(semiColon);
			
			pr.ast = new VarNameStmt(var, (Expr*) pr.ast);
		}
    }
    // Stmt ::= 'print' '(' Expr ')' ';'
    else if ( attemptMatch (printKwd) ) {
        match (leftParen) ;
        pr = parseExpr(0);
        match (rightParen) ;
        match (semiColon) ;
        
        pr.ast = new PrintStmt((Expr*) pr.ast);
    }
    // Stmt ::= 'for' '(' varName '=' Expr 'to' Expr ')' Stmt
    else if ( attemptMatch (forKwd) ) {
        match (leftParen) ;
        match (variableName) ;
        var = prevToken->lexeme.c_str();
        
        match (assign) ;
        pr = parseExpr (0) ;
        match (toKwd) ;
        pr1 = parseExpr (0) ;
        match (rightParen) ;
        pr2 = parseStmt () ;
        
        pr.ast = new ForStmt(var, (Expr*) pr.ast, (Expr*) pr1.ast, (Stmt*) pr2.ast);
    }
    // Stmt ::= ';'
    else if ( attemptMatch (semiColon) ) {
        // parsed a skip
        pr.ast = new SemiStmt();
    }
    else{
        throw ( makeErrorMsg ( currToken->terminal ) + " while parsing a statement" ) ;
    }
    // Stmt ::= variableName assign Expr semiColon
    return pr ;
}


/**
 * @see         Parser::parseExpr()
 * @brief       This method determines which Expr method to call.
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseExpr (int rbp) {
    ParseResult left = currToken->nud();
   
    while (rbp < currToken->lbp() ) {
        left = currToken->led(left) ;
    }

    return left ;
}

/*
 * parse methods for Expr productions
 * ----------------------------------
 */

/**
 * @see         Parser::parseIntConst()
 * @brief       This method matches integer constant.
 *              Expr ::= intConst
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseIntConst ( ) {
    ParseResult pr ;
    match ( intConst ) ;
    pr.ast = new Constant<string>(prevToken->lexeme);
    return pr ;
}


/**
 * @see         Parser::parseFloatConst()
 * @brief       This method matches float constant.
 *              Expr ::= floatConst
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseFloatConst ( ) {
    ParseResult pr ;
    match ( floatConst ) ;
    pr.ast = new Constant<string>(prevToken->lexeme);
    return pr ;
}

/**
 * @see         Parser::parseStringConst()
 * @brief       This method matches string constant.
 *              Expr ::= stringConst
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseStringConst ( ) {
    ParseResult pr ;
    match ( stringConst ) ;
    pr.ast = new Constant<string>(prevToken->lexeme);
    return pr ;
}


/**
 * @see         Parser::parseVariableName()
 * @brief       This method matches variable name, depending on the matched characters coming after.
 *              Expr ::= MatrixREF
 *              Expr ::= variableName '(' Expr ')' 
 *              Expr := variableName
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseVariableName ( ) {
    ParseResult pr,pr2;
    match ( variableName );
    string s = prevToken->lexeme.c_str();
    
    // This results matrix
    if(attemptMatch(leftSquare)){
		pr = parseExpr(0);
		match(comma);
		pr2 = parseExpr(0);
		match(rightSquare);
		pr.ast = new MatrixREF(s, (Expr*)pr.ast, (Expr*)pr2.ast);
	}
	//Expr ::= variableName '(' Expr ')'        //NestedOrFunctionCall
    else if(attemptMatch(leftParen)){
		pr = parseExpr(0);
		match(rightParen);
		pr.ast = new VarNameExpr(s, (Expr*)pr.ast);
	}
	//Expr := variableName
	else{
		pr.ast = new VarName(s);
	}
    return pr;
}

/**
 * @see         Parser::parseNestedExpr()
 * @brief       This method matches leftParen and rightParen and expressions within those parens.
 *              Expr ::= leftParen Expr rightParen
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseNestedExpr ( ) {
    ParseResult pr;
    match(leftParen);
    pr = parseExpr(0); 
    pr.ast = new lrParen((Expr*)pr.ast); 
    match(rightParen);
    return pr;
}

/**
 * @see         Parser::parseIfExpr()
 * @brief       This method matches if then else expression.
 *              Expr ::= 'if' Expr 'then' Expr 'else' Expr
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseIfExpr(){  
    ParseResult pr;
    ParseResult apr ; 
    ParseResult bpr;
    ParseResult cpr;
    
    match(ifKwd);
    apr = parseExpr(0);
    match(thenKwd);
	bpr = parseExpr(0);
    match(elseKwd);
    cpr = parseExpr(0);
	pr.ast = new IfThenElse((Expr*)apr.ast, (Expr*)bpr.ast, (Expr*)cpr.ast);
    return pr;
}

/**
 * @see         Parser::parseLetExpr()
 * @brief       This method matches let in end expression.
 *              Expr ::= 'let' Stmts 'in' Expr 'end'
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseLetExpr(){
	ParseResult pr,pr1 ;
	match(letKwd);
	pr = parseStmts();
	match(inKwd);
	pr1 = parseExpr(0);
	match(endKwd);
	pr.ast = new LetExpr((Stmts*)pr.ast,(Expr*)pr1.ast);
	
   return pr;
}

/**
 * @see         Parser::parseNotExpr()
 * @brief       This method matches not operator.
 *              Expr ::= '!' Expr
 * @param[in]   void
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseNotExpr () {
    ParseResult pr ;
    match ( notOp ) ;

    pr = parseExpr( 0 ); 
    pr.ast = new NotOp((Expr*)pr.ast);
    return pr ;
}

/**
 * @see         Parser::parseAddition(ParseResult)
 * @brief       This method matches addition operation.
 *              Expr ::= Expr plusSign Expr
 * @param[in]   ParseResult -- The input is an ParseResult object containing previous expression that will
 *                             be used as left operand for addition.
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
 ParseResult Parser::parseAddition ( ParseResult prLeft ) {
    // parser has already matched left expression 
    ParseResult pr ;
    match ( plusSign ) ;
    pr = parseExpr( prevToken->lbp() );
    pr.ast = new Addition((Expr*)prLeft.ast ,(Expr*)pr.ast);
    return pr ;
}

/**
 * @see         Parser::parseMultiplication(ParseResult)
 * @brief       This method matches multiplication operation.
 *              Expr ::= Expr star Expr
 * @param[in]   ParseResult -- The input is an ParseResult object containing previous expression that will
 *                             be used as left operand for multiplication.
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseMultiplication ( ParseResult prLeft ) {
    // parser has already matched left expression 
    ParseResult pr ;
    match ( star ) ;
    pr = parseExpr( prevToken->lbp() ); 
    pr.ast = new Multiplication((Expr*)prLeft.ast ,(Expr*)pr.ast);
    return pr ;
}

/**
 * @see         Parser::parseSubtraction(ParseResult)
 * @brief       This method matches subtraction operation.
 *              Expr ::= Expr dash Expr
 * @param[in]   ParseResult -- The input is an ParseResult object containing previous expression that will
 *                             be used as left operand for subtraction.
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseSubtraction ( ParseResult prLeft ) {
    // parser has already matched left expression 
    ParseResult pr ;
    match ( dash ) ;
    pr = parseExpr( prevToken->lbp() ); 
    pr.ast = new Subtraction((Expr*)prLeft.ast ,(Expr*)pr.ast);
    return pr ;
}

/**
 * @see         Parser::parseDivision(ParseResult)
 * @brief       This method matches division operation.
 *              Expr ::= Expr forwardSlash Expr
 * @param[in]   ParseResult -- The input is an ParseResult object containing previous expression that will
 *                             be used as left operand for division.
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseDivision ( ParseResult prLeft ) {
    // parser has already matched left expression 
    ParseResult pr ;
    match ( forwardSlash ) ;
    pr = parseExpr( prevToken->lbp() ); 
    pr.ast = new Division((Expr*)prLeft.ast ,(Expr*)pr.ast);
    return pr ;
}

/**
 * @see         Parser::parseRelationalExpr(ParseResult)
 * @brief       This method matches any relational expression.
 *              Expr ::= Expr equalEquals Expr
 *              Expr ::= Expr lessThanEquals Expr
 *              Expr ::= Expr greaterThanEquals Expr
 *              Expr ::= Expr notEquals Expr
 *              Expr ::= Expr leftAngle Expr
 *              Expr ::= Expr rightAngle Expr
 * @param[in]   ParseResult -- The input is an ParseResult object containing previous expression that will
 *                             be used as left operand for any relational operation.
 * return       ParseResult -- The output is an ParseResult object containing error message (if there was)
 *                             and parsed object.
 */
ParseResult Parser::parseRelationalExpr ( ParseResult prLeft ) {
    ParseResult pr ;
    nextToken() ;
    string op = prevToken->lexeme ;
    pr = parseExpr( prevToken->lbp() );
    pr.ast = new RelationalExpr((Expr*)prLeft.ast ,(Expr*)pr.ast, op);
    return pr ;
}


/** Helper functions used by the parser.*/
void Parser::match (tokenType tt) {
    if (! attemptMatch(tt)) {
        throw ( makeErrorMsgExpected ( tt ) ) ;
    }
}

bool Parser::attemptMatch (tokenType tt) {
    if (currToken->terminal == tt) { 
        nextToken() ;
        return true ;
    }
    return false ;
}

bool Parser::nextIs (tokenType tt) {
    return currToken->terminal == tt ;
}

void Parser::nextToken () {
    if ( currToken == NULL ) 
        throw ( string("Internal Error: should not call nextToken in unitialized state"));
    else 
    if (currToken->terminal == endOfFile && currToken->next == NULL) {
        prevToken = currToken ;
    } else if (currToken->terminal != endOfFile && currToken->next == NULL) {
        throw ( makeErrorMsg ( "Error: tokens end with endOfFile" ) ) ;
    } else {
        prevToken = currToken ;
        currToken = currToken->next ;
    }
}

string Parser::terminalDescription ( tokenType terminal ) {
    Token *dummyToken = new Token ("",terminal, NULL, 1, 1) ;
    ExtToken *dummyExtToken = extendToken (this, dummyToken) ;
    string s = dummyExtToken->description() ;
    delete dummyToken ;
    delete dummyExtToken ;
    return s ;
}

string Parser::makeErrorMsgExpected ( tokenType terminal ) {
    string line, column;
    ostringstream ss;
    ss << currToken->line;
    line = ss.str();

    ostringstream st;
    st << currToken->column;
    column = st.str();

    string s = (string) "Expected " + terminalDescription (terminal) +
        " but found " + currToken->description() + ". The error happened at line " + line + " column " + column;
    
    return s;
}

string Parser::makeErrorMsg ( tokenType terminal ) {
    string line, column;
    ostringstream ss;
    ss << currToken->line;
    line = ss.str();

    ostringstream st;
    st << currToken->column;
    column = st.str();

    string s = "Unexpected symbol " + terminalDescription (terminal) + ". The error happened at line " + 
        line + " column " + column;
    
    return s;
}

string Parser::makeErrorMsg ( const char *msg ) {
    return msg ;
}
