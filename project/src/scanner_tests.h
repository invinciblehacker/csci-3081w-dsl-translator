#include <cxxtest/TestSuite.h>

#include "readInput.h"
#include "scanner.h"
#include "regex.h"
#include <stdio.h>
#include <string>
#include <iostream>
using namespace std ;

class ScannerTestSuite : public CxxTest::TestSuite 
{
public:
    /*! A Scanner object is created in the test_setup_code method.
       This is a test, beginning with "test_", so that it is executed
       by the testing framework.  The scanner is used in some tests of
       the method "scan".
     */
    Scanner *s ;
    void test_setup_code ( ) {
        s = new Scanner() ;
    }   


    // Tests for components and functions used by "scan"
    // --------------------------------------------------

    /* You may need to write several tests to adequately test the
       functions that are called from "scan".  Once you are confident
       that the components used by "scan" work properly, then you can
       run tests on the "scan" method itself.
    */

    void test_consumeWhitespaceAndComments () 
    {
        const char *testInput = " /*ABC*/abcabc";
        int testValue = s->consumeWhiteSpaceAndComments(testInput);
        testInput += testValue;
        
        TS_ASSERT_EQUALS(testValue, 8);
        TS_ASSERT_EQUALS(testInput, "abcabc");
    }

    void test_getNextTokenLex()
    {
        const char *testInput = "abcde";
        string test = s->getNextTokenLex(testInput, 3);

        TS_ASSERT_EQUALS( test.compare("abc") , 0 );
    }
    

    /* Below is one of the tests for these components in the project
       solution created by your instructor..  It uses a helper
       function function called "tokenMaker_tester", which you have
       not been given.  You are expected to design your own components
       for "scan" and your own mechanisms for easily testing them.

        void xtest_TokenMaker_leftCurly ( ) {
            tokenMaker_tester ("{ ", "^\\{", leftCurly, "{" ) ;
        }

        Note that this test is here named "xtest_Token..." The leading
        "x" is so that cxxTest doesn't scan the line above and think
        it is an actual test that isn't commented out.  cxxTest is
        very simple and doesn't even process block comments.
    */


    /* You must have at least one separate test case for each terminal
       symbol.  Thus, you need a test that will pass or fail based
       solely on the regular expression (and its corresponding code)
       for each terminal symbol.

       This requires a test case for each element of the enumerated
       type tokenType.  This may look something like the sample test
       shown in the comment above.
    */

    
    void getNextTokenTerminal_tester (const char* testText, tokenType type, int inLength)
    {
        Token* temp = new Token();
        int lexLength = 0;
        temp->terminal = s->getNextTokenTerminal(testText, lexLength);

        TS_ASSERT_EQUALS(lexLength, inLength);
        TS_ASSERT_EQUALS(temp->terminal, type);
        delete temp;
    }


    void test_getNextTokenTypeTerminal_intKwd(){
        getNextTokenTerminal_tester("Integer", intKwd, 7);
    }
    void test_getNextTokenTypeTerminal_floatKwd () {
        getNextTokenTerminal_tester("Float", floatKwd, 5);
    }
    void test_getNextTokenTypeTerminal_stringKwd () {
        getNextTokenTerminal_tester("String", stringKwd, 6);
    }
    void test_getNextTokenTypeTerminal_matrixKwd () {
        getNextTokenTerminal_tester("Matrix", matrixKwd, 6);
    }
    void test_getNextTokenTypeTerminal_letKwd () {
        getNextTokenTerminal_tester("let", letKwd, 3);
    }
    void test_getNextTokenTypeTerminal_inKwd () {
        getNextTokenTerminal_tester("in", inKwd, 2);
    }
    void test_getNextTokenTypeTerminal_endKwd () {
        getNextTokenTerminal_tester("end", endKwd, 3);
    }
    void test_getNextTokenTypeTerminal_ifKwd () {
        getNextTokenTerminal_tester("if", ifKwd, 2);
    }
    void test_getNextTokenTypeTerminal_thenKwd () {
        getNextTokenTerminal_tester("then", thenKwd, 4);
    }
    void test_getNextTokenTypeTerminal_elseKwd () {
        getNextTokenTerminal_tester("else", elseKwd, 4);
    }
    void test_getNextTokenTypeTerminal_forKwd () {
        getNextTokenTerminal_tester("for", forKwd, 3);
    }
    void test_getNextTokenTypeTerminal_toKwd () {
        getNextTokenTerminal_tester("to", toKwd, 2);
    }
    void test_getNextTokenTypeTerminal_printKwd () {
        getNextTokenTerminal_tester("print", printKwd, 5);
    }
    void test_getNextTokenTypeTerminal_intConst () {
        getNextTokenTerminal_tester("18273", intConst, 5);
    }
    void test_getNextTokenTypeTerminal_floatConst () {
        getNextTokenTerminal_tester("12.3", floatConst, 4);
    }
    void test_getNextTokenTypeTerminal_stringConst () {
        getNextTokenTerminal_tester("\"abc\"", stringConst, 5);
    }
    void test_getNextTokenTypeTerminal_variableName () {
        getNextTokenTerminal_tester("d_S1", variableName, 4);
    }
    void test_getNextTokenTypeTerminal_leftParen () {
        getNextTokenTerminal_tester("(", leftParen, 1);
    }
    void test_getNextTokenTypeTerminal_rightParen () {
        getNextTokenTerminal_tester(")", rightParen, 1);
    }
    void test_getNextTokenTypeTerminal_leftCurly () {
        getNextTokenTerminal_tester("{", leftCurly, 1);
    }
    void test_getNextTokenTypeTerminal_rightCurly () {
        getNextTokenTerminal_tester("}", rightCurly, 1);
    }
    void test_getNextTokenTypeTerminal_leftSquare () {
        getNextTokenTerminal_tester("[", leftSquare, 1);
    }
    void test_getNextTokenTypeTerminal_rightSquare () {
        getNextTokenTerminal_tester("]", rightSquare, 1);
    }
    void test_getNextTokenTypeTerminal_comma () {
        getNextTokenTerminal_tester(",", comma, 1);
    }
    void test_getNextTokenTypeTerminal_semiColon () {
        getNextTokenTerminal_tester(";", semiColon, 1);
    }
    void test_getNextTokenTypeTerminal_assign () {
        getNextTokenTerminal_tester("=", assign, 1);    
    }
    void test_getNextTokenTypeTerminal_plusSign () {
        getNextTokenTerminal_tester("+", plusSign, 1);
    }
    void test_getNextTokenTypeTerminal_dash () {
        getNextTokenTerminal_tester("-", dash, 1);
    }
    void test_getNextTokenTypeTerminal_forwardSlash () {
        getNextTokenTerminal_tester("/", forwardSlash, 1);
    }
    void test_getNextTokenTypeTerminal_lessThan () {
        getNextTokenTerminal_tester("<", lessThan, 1);
    }
    void test_getNextTokenTypeTerminal_lessThanEqual () {
        getNextTokenTerminal_tester("<=", lessThanEqual, 2);    
    }
    void test_getNextTokenTypeTerminal_greaterThan () {
        getNextTokenTerminal_tester(">", greaterThan, 1); 
    }
    void test_getNextTokenTypeTerminal_greaterThanEqual () {
        getNextTokenTerminal_tester(">=", greaterThanEqual, 2); 
    }
    void test_getNextTokenTypeTerminal_equalsEquals () {
        getNextTokenTerminal_tester("==", equalsEquals, 2); 
    }
    void test_getNextTokenTypeTerminal_notEquals () {
        getNextTokenTerminal_tester("!=", notEquals, 2); 
    }
    void test_getNextTokenTypeTerminal_andOp () {
        getNextTokenTerminal_tester("&&", andOp, 2); 
    }
    void test_getNextTokenTypeTerminal_orOp () {
        getNextTokenTerminal_tester("||", orOp, 2); 
    }
    void test_getNextTokenTypeTerminal_notOp () {
        getNextTokenTerminal_tester("!", notOp, 1);  
    }


    // Tests for "scan"
    // --------------------------------------------------

    /* Below are some helper functions and sample tests for testing the 
       "scan" method on Scanner.
    */

    /*!Test that a list of tokens has no lexicalError tokens.*/
    bool noLexicalErrors (Token *tks) {
        Token *currentToken = tks ;
        while (currentToken != NULL) {
            if (currentToken->terminal == lexicalError) {
                printf("problem: %s\n",currentToken->lexeme.c_str());
				return false ;
            }
            else {
                currentToken = currentToken->next ;
            }
        }
        return true ;
    }

    /*! A quick, but inaccurate, test for scanning files.  It only
       checks that no lexical errors occurred, not that the right
       tokens were returned. 
    */
    void scanFileNoLexicalErrors ( const char* filename ) {
        char *text =  readInputFromFile ( filename )  ;
        TS_ASSERT ( text ) ;
        Token *tks = s->scan ( text ) ;
        TS_ASSERT (tks != NULL) ;
        TS_ASSERT (noLexicalErrors(tks));
    }


    /*! This function checks that the terminal fields in the list of
       tokens matches a list of terminals.
    */
    bool sameTerminals (Token *tks, int numTerms, tokenType *ts) {
        Token *currentToken = tks ;
        int termIndex = 0 ;
        while (currentToken != NULL && termIndex < numTerms ) {
            if (currentToken->terminal != ts[termIndex]) {
            printf("%i: %i should be %i\n",termIndex,currentToken->terminal , ts[termIndex]);
			fflush(stdout);
                return false ;
            }
            else {
                ++ termIndex ;
                currentToken = currentToken->next ;
            }
        }
        return true ;
    }


    /* Below are the provided test files that your code should also
       pass.

       You may initially want to rename these tests to "xtest_..." so
       that the CxxTest framework does not see it as a test and won't
       run it as one.  This way you can focus on the tests that you'll
       write above for the individual terminal types first. Then focus
       on these tests.

    */

    /*!The "endOfFile" token is always the last one in the list of tokens.*/
    void test_scan_empty ( ) {
        Token *tks = s->scan ("  ") ;
        TS_ASSERT (tks != NULL) ;
        TS_ASSERT_EQUALS (tks->terminal, endOfFile) ;
        TS_ASSERT (tks->next == NULL) ;
    }

    void test_scan_empty_comment ( ) {
        Token *tks = s->scan (" /* a comment */ ") ;
        TS_ASSERT (tks != NULL) ;
        TS_ASSERT_EQUALS (tks->terminal, endOfFile) ;
        TS_ASSERT (tks->next == NULL) ;
    }



    /*!When a lexical error occurs, the scanner creates a token with a 
    single-character lexeme and lexicalError as the terminal.
    */
   void test_scan_lexicalErrors ( ) {
        Token *tks = s->scan ("$&1  ") ;
        TS_ASSERT (tks != NULL) ;
        TS_ASSERT_EQUALS (tks->terminal, lexicalError) ;
        TS_ASSERT_EQUALS (tks->lexeme, "$");
        tks = tks->next ;
        TS_ASSERT (tks != NULL) ;
        TS_ASSERT_EQUALS (tks->terminal, lexicalError) ;
        TS_ASSERT_EQUALS (tks->lexeme, "&");
        tks = tks->next ;
        TS_ASSERT (tks != NULL) ;
        TS_ASSERT_EQUALS (tks->terminal, intConst) ;
        TS_ASSERT_EQUALS (tks->lexeme, "1");
        tks = tks->next ;
        TS_ASSERT (tks != NULL) ;
        TS_ASSERT_EQUALS (tks->terminal, endOfFile) ;
        TS_ASSERT (tks->next == NULL) ;
    }


    /*!A test for scanning numbers and a variable.*/
    void test_scan_nums_vars ( ) {
        Token *tks = s->scan (" 123 x 12.34 ") ;
        TS_ASSERT (tks != NULL) ;
        tokenType ts[] = { intConst, variableName, floatConst, endOfFile } ;
        TS_ASSERT ( sameTerminals ( tks, 4, ts ) ) ;
    }

    void test_squarebracket ( ) {
        Token *tks = s->scan (" [10,10] ") ;
        TS_ASSERT (tks != NULL) ;
        tokenType ts[] = { leftSquare, intConst, comma, intConst, rightSquare } ;
        TS_ASSERT ( sameTerminals ( tks, 5, ts ) ) ;
    }

    /*! This test checks that the scanner returns a list of tokens with
       the correct terminal fields.  It doesn't check that the lexeme
       are correct.
     */
    void test_scan_bad_syntax_good_tokens ( ) {
        const char *filename = "../samples/bad_syntax_good_tokens.dsl" ;
        char *text =  readInputFromFile ( filename )  ;
        TS_ASSERT ( text ) ;
        Token *tks = s->scan ( text ) ;
        TS_ASSERT (tks != NULL) ;
        tokenType ts[] = { 
            intConst, intConst, intConst, intConst, 

            stringConst, stringConst, stringConst,

            floatConst, floatConst, floatConst,

			matrixKwd,



            semiColon, comma, 
            leftCurly, leftParen, rightCurly, rightParen,

            plusSign, star, dash, forwardSlash, 

            equalsEquals, lessThanEqual, 
            greaterThanEqual, notEquals, 
            assign,

            variableName, variableName, variableName, variableName, 
            variableName, variableName, variableName,


            intKwd, floatKwd,  stringKwd, 

            endOfFile
       } ;
        int count = 37; 
        TS_ASSERT ( sameTerminals ( tks, count, ts ) ) ;
    }

    void test_scan_sample_forestLoss ( ) {
        scanFileNoLexicalErrors ("../samples/forest_loss_v2.dsl") ;
    }

      

} ;
