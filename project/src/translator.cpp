#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include "readInput.h"
#include "parser.h"
#include "../samples/Matrix.h"

using namespace std;

/*!
 * This is a program that takes a DSL program as input and translates it into a C++ program.
 * The C++ code file is put into 'program.cpp', and the executable file is put
 * into 'program'.
*/

int main(){
	ofstream outfile;
	string filename;
	int rc = 0;
	
	cout << "Enter the dsl file name to translate (without path): ";
	cin >> filename;

	string fullpath = "../samples/" + filename;
	
	Parser *p = new Parser();
	ParseResult pr = p->parse(readInputFromFile(fullpath.c_str()));
	
	// 1. Test that the file can be parsed.
	if(!pr.ok){
		cout << pr.errors << endl;
		exit(1);
	}
	
	// 2. Verify that the ast field is not null
	if(pr.ast == NULL){
		cout << "Failed to generate an AST." << endl;
		exit(1);
	}

	// 3. Verify that the C++ code is non-empty.
	string cpp = pr.ast->cppCode();
	if(cpp.length() <= 0){
		cout << "Failed to generate non-empty C++ code." << endl;
		exit(1);
	}

	// Write file
	outfile.open("../samples/program.cpp");
	outfile << (string) cpp << endl;
	outfile.close();
	
	// 4. Compile generated C++ file
    string compile = "g++ ../samples/Matrix.cpp ../samples/program.cpp -o ../samples/program";
    rc = system(compile.c_str());
    if(rc == 0){
    	cout << "Translation had compiled without errors." << endl;
    	exit(1);
    }
    
    string cleanup = "rm -f ../samples/program.output";
    system (cleanup.c_str()) ;
    
    // Get filename without extension
    size_t position = filename.find(".");
    string onlyName = (string::npos == position) ? filename : filename.substr(0, position);

    // 5. Run the generated code.
    string run = "../samples/program > ../samples/program.output";
    rc = system ( run.c_str() ) ;
  	if(rc == 0){
    	cout << "Translation executed without errors." << endl;
    	exit(1);
    }

	return 0;
}
