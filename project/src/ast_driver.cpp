#include <iostream> 
#include "parser.h"
#include "readInput.h"

#include <stdlib.h>
#include <string.h>
#include <fstream>

using namespace std;

/*!
 * This file is used to test any implements we made right away. 
 * This is useful since we used bottom-up design.
 */
int main() {
  Parser *p = new Parser();
  const char *filename = "../samples/mysample.dsl" ;
  ParseResult pr = p->parse ( readInputFromFile (filename) ) ;
  if(!pr.ok) {
    cout << "FAILURE TO PARSE!\n";
  }
  if(!pr.ast) {
    cout << "EMPTY AST!\n";
    return 1;
  }
  cout<< pr.ast->unparse() <<endl;
  return 0;
}
