On this iteration we mostly worked together on constructing all the classes. We found that we produced much less buggy code this way, wrote more concise code, and got a better understanding of our code.

Johnny had oversight of the documentation and made sure everything was done according to the rubric, while Torry designed the UML diagrams. We both evaluated each others work.