##
Iteration 2

We worked on the makefile together. We both had hard time writing makefile
since we were confused when we looked at the complex code of makefile. If the makefile
wasn't that complex, we could have finished a lot faster. Anyway, we worked on it together 
all the way.

When we got the dependencies working, there was still some minor issues with regexes when we ran the parser tests. We fixed that pretty quickly.
