/*! \mainpage DSL translator

\section intro_sec Introduction

This is a DSL translator that takes in a file that contains DSL code and translates it into C++ code.
This build is modified by Janyoog Kim and Torry Tufteland as the course project in 3081 - Program Design and Development.

\section install_sec Installation

The program is not complete as it is now, and will only be able to parse the code it is given. We have not started on the
actual translating part yet.



*/
