#ifndef SCANNER_H
#define SCANNER_H
#include "regex.h"
#include <string>
#include <vector>

using namespace std;

class Token ;
class Scanner ;

/*! This enumerated type is used to keep track of what kind of
   construct was matched. 
 */
enum tokenEnumType { 

	intKwd, floatKwd,  stringKwd, matrixKwd,
	letKwd, inKwd, endKwd, ifKwd, thenKwd, elseKwd,
	toKwd, forKwd, printKwd,

	// Constants
	intConst, floatConst, stringConst, 

	// Names
	variableName ,

	// Punctuation
	leftParen, rightParen, 
	leftCurly, rightCurly, 
	leftSquare, rightSquare,

	comma, semiColon,

	//Operators
	assign, 
	plusSign, star, dash, forwardSlash,
	lessThan, lessThanEqual, greaterThan, greaterThanEqual,
	equalsEquals, notEquals,
	andOp, orOp, notOp,

	// Special terminal types
	endOfFile ,
	lexicalError
} ;
typedef enum tokenEnumType tokenType ;


/*!Below are our class definitions for Token and Scanner. */
class Token{
public:
    tokenType terminal;
    string lexeme;
    Token *next;
    int line;
    int column;

    Token(){ terminal = endOfFile; }
    Token(string, tokenType, Token*, int, int) ;
};


class Scanner{
public:
	vector<regex_t*> regexList;
	Token* head;
    Token* scan(const char*);

    Scanner();
    ~Scanner();

    int consumeWhiteSpaceAndComments (const char*, int&, int&);
    int consumeWhiteSpaceAndComments (const char*);
    Token* getNextToken(const char*, int&, int&, int&, int&);
    tokenType getNextTokenTerminal(const char*, int&);
    string getNextTokenLex(const char*, int);
};


#endif /* SCANNER_H */
