/*
 * FCA language program 2
 * This was created by Jangyoon Kim and Torry Tufteland
 * 
 */

main(){ 
    Integer index;
    index = 7;
    Matrix m[10, 10] i, j = 
      let
          Float init;
          Float diff;
          diff = index - i - j;
          if(diff >= 0)
              init = diff;
          else
              init = diff * (0.0 - 1);
      in 
        init
      end;

    print(m);

}
