/*
 * FCA language program 1
 * This was created by Jangyoon Kim and Torry Tufteland
 *
 */

main(){
	Integer i; 
	i = 1;
	Integer j;
	j = 1;

	Matrix m1[10, 10] a, b = a * b - 5;
	Matrix m2[10, 10] c, d = c + d;
	Matrix m3[10, 10] k, l = 
		let 
			Integer ps;
			ps = m1[k, l];
			ps = 
				if ps > 0 
				then m2[k, l]
				else m2[l, k];
		in 
			ps
		end;

	print(m3);
}
