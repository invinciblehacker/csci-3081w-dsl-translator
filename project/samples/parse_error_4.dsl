/*
 * The error of not using let statement correctly should occur at line 11 column 5
 * and the terminal should be letKwd.
 */

main(){
	Integer x;
	Integer y;
	x = 3;

	let 
		y = 4;
		y = y + x;
	in
		Integer z;
	end;
}