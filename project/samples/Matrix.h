#ifndef MATRIX_H
#define MATRIX_H

#include <stdlib.h>
#include <iostream>
#include <fstream>
using namespace std;

class Matrix {
	public:
  	Matrix(int, int);
  	Matrix(const Matrix&);
  	~Matrix();

  	int numRows() const;
    int numCols() const;
    friend int numRows(Matrix);
    friend int numCols(Matrix);

    float* access(const int, const int) const;
    friend ostream& operator<<(ostream &out, Matrix m);

    static Matrix readMatrix(string filename);

	private:
    Matrix(){}
    int rows;
    int cols;

    /* Your implementation of "data" may vary.  There are ways in
       which data can be an array of arrays and thus simplify the
       access method, at the cost of complicating the process of
       allocating space for data.  The choice is entirely up to
       you. */
    float **data;
};

#endif // MATRIX_H
