#include <stdlib.h>
#include <iostream>
#include <fstream>
#include "Matrix.h"
using namespace std;

Matrix::Matrix(int i, int j) {
	rows = i;
	cols = j;
	data = new float*[rows];
	for(int k = 0; k < rows; k++){
		data[k] = new float[cols];
	}
}

Matrix::Matrix (const Matrix& m){
	rows = m.numRows();
	cols = m.numCols();
	data = new float*[rows];
	for(int k = 0; k < rows; k++){
		data[k] = new float[cols];
	}

	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			data[i][j] = *m.access(i, j);
		}
	}
}

Matrix::~Matrix(){
	rows = 0;
	cols = 0;
	
	for(int i = 0; i < rows; i++){
		delete [] data[i];
	}
	delete [] data;
}

int Matrix::numRows() const{
	return rows;
}

int Matrix::numCols() const{
	return cols;
}

int numRows(Matrix m){
	return m.numRows();
}

int numCols(Matrix m){
	return m.numCols();
}

float* Matrix::access(const int i, const int j) const {
	return &data[i][j];
}

ostream& operator<<(ostream &out, Matrix m){
	int mrows = m.numRows();
	int mcols = m.numCols();
	out << mrows << " " << mcols << endl;
	
	for (int i = 0; i < mrows; i++){
		for (int j = 0; j < mcols; j++){
			out << *m.access(i, j) << "  ";
		}
		out << endl;
	}
	
	return out;
}

Matrix Matrix::readMatrix (string filename){
	int rows = 0, cols = 0;
	char *file_name = (char*)filename.c_str();
	ifstream infile;
	infile.open(file_name);
	infile >> rows >> cols;
	Matrix m(rows, cols);
	
	for (int i = 0; i < rows; i++){
		for (int j = 0; j < cols; j++){
			infile >> m.data[i][j];
		}
	}
	
	infile.close();
	
	return m;
}
